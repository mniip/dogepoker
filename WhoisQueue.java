package irccasino;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.WhoisEvent;

class WhoisRequest extends CountDownLatch {
	private String nickname;
	private String account;
	public WhoisRequest(String nick) {
		super(1);
		nickname = nick;
	}

	public String getNick() {
		return nickname;
	}

	public void complete(String acc) {
		account = acc;
		countDown();
	}

	public String waitCompletion() throws InterruptedException {
		await();
		return account;
	}
}

public class WhoisQueue extends ListenerAdapter<PircBotX> {
	private GameManager manager;
	private BlockingQueue<WhoisRequest> queue;

	public WhoisQueue(GameManager parent)
	{
		manager = parent;
		queue = new ArrayBlockingQueue<WhoisRequest>(1024);
	}

	public void onWhois(WhoisEvent<PircBotX> event) throws IllegalStateException {
		WhoisRequest w = queue.poll();
		if(w == null)
			throw new IllegalStateException("Got whois reply for " + event.getNick() + " but nothing queued");
		if(!event.getNick().equalsIgnoreCase(w.getNick()))
			throw new IllegalStateException("Whois queued " + w.getNick() + " but got " + event.getNick());
		w.complete(event.getRegisteredAs());
	}

	public String whois(String nickname) {
		try {
			WhoisRequest w = new WhoisRequest(nickname);
			synchronized(queue) {
				queue.put(w);
				manager.sendRawLine("WHOIS :" + nickname);
			}
			return w.waitCompletion();
		} catch(InterruptedException e) {
			return null;
		}
	}
}
